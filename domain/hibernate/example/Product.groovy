package hibernate.example


class Product {
    String name
    String unit
    Double unitCost
    Double amount
    Double totalCost
    String month

    static constraints = {
        name blank:false
        unit blank:false
    }
}
