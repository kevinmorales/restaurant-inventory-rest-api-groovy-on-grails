package hibernate.example

class UrlMappings {

    static mappings = {

        /*
        delete "/$controller/$id(.$format)?"(action:"delete")
        get "/$controller(.$format)?"(action:"index")
        get "/$controller/$id(.$format)?"(action:"show")
        post "/$controller(.$format)?"(action:"save")
        put "/$controller/$id(.$format)?"(action:"update")
        patch "/$controller/$id(.$format)?"(action:"patch")




            '/products'(resources: 'product') {
            collection {
                '/$month'(controller: 'product', action: 'getByMonth')
                '/$month/$id'(controller: 'product', action: 'getByID')
            }
        }

                    */
        "/"(controller: 'application', action:'index')
        get "/products"(controller: 'product', action: 'index')
        get "/products/$month"(controller: 'product', action: 'getByMonth')
        get "/products/$month/$id"(controller: 'product', action: 'getById')
        post "/products/$month"(controller: 'product', action: 'addProduct')
        put "/products/$month/$id"(controller: 'product', action: 'updateProduct')
        delete "/products/$month/$id"(controller: 'product', action: 'deleteProduct')

        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
