package hibernate.example


import grails.rest.*
import grails.converters.*

class ProductController extends RestfulController {
    static responseFormats = ['json', 'xml']
    ProductController() {
        super(Product)
    }
    //Months in the year where inventory is held
    String [] months = ["january", "february", "march", "april", "may", "june",
                        "july", "august", "september", "october", "november", "december"]
    //GET Request for getting all inventory in the database
    def index() {
        //SQL -> SELECT * FROM Product
        def query = "from Product as myProduct"
        //IF the query does not return an empty set
        if(!(Product.findAll(query) as String == "[]" )) {
            //TRY to query the database
            try {
                //RENDER the result of the query as JSON
                render Product.findAll(query) as JSON
            }
            //CATCH Exceptions
            catch (Exception e) {
                println(e)
                //RENDER failure message
                render "{ \"success\": false }"
            }//END TRY/CATCH
        }
        //ELSE the query returned an empty set
        else {
            response.status = 404
            //RENDER a failure message
            render "{ \"success\": false, \"reason\": \"There is no inventory to display\" }"
        }
    }

    //GET Request for getting all inventory from a given month
    def getByMonth(){

        def uriMonth = request.forwardURI.split("/")[-1]  //get month from uri
        //SQL -> SELECT * from Product WHERE month = uriMonth
        def query = "from Product as myProduct where myProduct.month=?0"

        //If the URI parameter is a valid month
        if(months.contains(uriMonth.toLowerCase())) {
            //IF the query does not return an empty set
            if(!(Product.findAll(query, [uriMonth]) as String == "[]" )) {
                try {
                    render Product.findAll(query, [uriMonth]) as JSON
                }
                catch (Exception e) {
                    println(e)
                    render "{ \"success\": false }"
                }
            }
            else{
                response.status = 404
                render "{ \"success\": false, \"reason\": \"" + uriMonth + " has no inventory\" }"
            }
        }
        else{
            response.status = 404
            render "{ \"success\": false }"
        }
    }

    //GET request to get inventory by id number
    def getById(){
        //Get the month from the uri
        def uriMonth = request.forwardURI.split("/")[-2]
        //Get the id from the uri
        def uriId = request.forwardURI.split("/")[-1] as long

        //SQL -> SELECT * FROM Product WHERE month = uriMonth AND id = uriId
        def query = "from Product as myProduct where myProduct.month=?0 and myProduct.id=?1"

        //If the URI parameter is a valid month
        if(months.contains(uriMonth.toLowerCase())) {
            //IF the query does not return a empty set
            if(!(Product.findAll(query, [uriMonth, uriId]) as String == "[]" )) {
                //TRY to query the database
                try {
                    //RENDER the result set
                    render Product.findAll(query, [uriMonth, uriId]) as JSON
                }
                //CATCH Exceptions
                catch (Exception e) {
                    println(e)
                    //RENDER a failure message
                    render "{ \"success\": false }"
                }
            }
            //ELSE the query returned an empty set
            else{
                response.status = 404
                //RENDER a failure message
                render "{ \"success\": false }"
            }//END IF
        }
        //ELSE the month parameter is not a valid month
        else {
            response.status = 404
            //RENDER a failure message
            render "{ \"success\": false }"
        }//END IF
    }//END getById

    //POST request to add to inventory
    def addProduct(){
        //Get request JSON
        def reqJson = request.JSON
        //Get month from uri
        def uriMonth = request.forwardURI.split("/")[-1]  //get month from uri

        //IF the URI parameter is a valid month
        if(months.contains(uriMonth.toLowerCase())) {
            //TRY to add the product
            try {
                //Create a new product from the JSON parameters
                def myProduct = new Product(
                        name: reqJson.name,
                        unit: reqJson.unit,
                        unitCost: reqJson.unitCost as double,
                        amount: reqJson.amount as double,
                        month: uriMonth
                )

                //Calculate total cost based on new values
                myProduct.totalCost = myProduct.amount * myProduct.unitCost

                //Save the product to the database
                myProduct.save()
                //Send a success message
                render "{ \"success\": true }"
            //CATCH exceptions
            }catch (Exception ex) {
                //Send a failure message
                render "{ \"success\": false }"
            }//END TRY/CATCH
        }
        //ELSE the month parameter is not a valid month
        else {
            response.status = 404
            //RENDER a failure message
            render "{ \"success\": false }"
        }//END IF
    }

    //PUT Request for editing inventory entries
    def updateProduct(){
        //Get request JSON
        def reqJson = request.JSON
        //Get month from uri
        def uriMonth = request.forwardURI.split("/")[-1]
        //Get id from uri
        def uriId = request.forwardURI.split("/")[-1] as long
        //SQL -> SELECT * FROM Product WHERE month = uriMonth AND id = uriId
        def query = "from Product as myProduct where myProduct.month=?0 and myProduct.id=?1"

        //If the URI parameter is a valid month
        if(months.contains(uriMonth.toLowerCase())) {
            //IF the query does not return a empty set
            if(!(Product.findAll(query, [uriMonth, uriId]) as String == "[]" )) {
                //TRY to edit the inventory entry
                try {
                    //Get the product with the parameter id
                    def myProduct = Product.get(uriId)

                    if(reqJson.name != null)
                        myProduct.name = reqJson.name
                    if(reqJson.unit != null)
                        myProduct.unit = reqJson.unit
                    if(reqJson.unitCost != null)
                        myProduct.unitCost = reqJson.unitCost as double
                    if(reqJson.amount != null)
                        myProduct.amount = reqJson.amount as double

                    //Update total cost based on potential new values
                    myProduct.totalCost = myProduct.amount * myProduct.unitCost

                    //SAVE the edited product to the database
                    myProduct.save(flush: true)

                    //RENDER a success message
                    render "{ \"success\": true }"

                } catch (Exception ex) {
                    println(ex)
                    render "{ \"success\": false }"
                }
            }
            //ELSE the query returned an empty set
            else{
                response.status = 404
                //RENDER a failure message
                render "{ \"success\": false }"
            }//END IF
        }
        //ELSE the month parameter is not a valid month
        else {
            response.status = 404
            //RENDER a failure message
            render "{ \"success\": false }"
        }//END IF
    }

    def deleteProduct(){
        //Get month from uri
        def uriMonth = request.forwardURI.split("/")[-2]  //get month from uri
        //Get id from uri
        def uriId = request.forwardURI.split("/")[-1] as long
        //SQL -> SELECT * FROM Product WHERE month = uriMonth AND id = uriId
        def query = "from Product as myProduct where myProduct.month=?0 and myProduct.id=?1"
        //If the URI parameter is a valid month
        if(months.contains(uriMonth.toLowerCase())) {
            //IF the query does not return a empty set
            if(!(Product.findAll(query, [uriMonth, uriId]) as String == "[]" )) {
                //TRY to query the database
                try {
                    //Return the product that is about to be deleted
                    render Product.findAll(query, [uriMonth, uriId]) as JSON

                    //DELETE the product from the inventory
                    def myProduct = Product.findById(uriId)
                    myProduct.delete(flush:true)
                }
                //CATCH Exceptions
                catch (Exception e) {
                    println(e)
                    //RENDER a failure message
                    render "{ \"success\": false, \"id\": " + uriId + "}"
                }
            }
            //ELSE the query returned an empty set
            else{
                response.status = 404
                //RENDER a failure message
                render "{ \"success\": false, \"reason\":\"No Products with id of " + uriId + "\"}"
            }//END IF
        }
        //ELSE the month parameter is not a valid month
        else {
            response.status = 404
            //RENDER a failure message
            render "{ \"success\": false, \"reason\":\"" + uriMonth +  "is not a valid month\"}"
        }//END IF
    }
}
